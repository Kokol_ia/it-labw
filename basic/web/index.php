<!DOCTYPE html>
<html>
<head>
	<title>map</title>
	<link rel="stylesheet" href="style/css/map.css">
</head>
<body>
	<div id="map">
		<div id="size-controller">
			<img src="style/img/plus.png" alt="">
			<img src="style/img/minus.png" alt="">
		</div>
		<div id="cc">
			<img class="img-svg" src="style/img/russia.svg" alt="">
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="style/js/map.js"></script>
</body>
</html>